/*
 * Opapi Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2022 <esodan@gmail.com>
 *
 * libopapi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liboapi is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Tests.UnitTest {
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Test.init (ref args);
        Test.add_func ("/opapi/request/project/form",
        ()=>{
            string server = "https://community.openproject.com";
            string[] es = GLib.Environ.get ();

            string eserver = GLib.Environ.get_variable (es, "OPAPI_SERVER");
            if (eserver != null) {
                server = eserver;
            }

            string eapikey = GLib.Environ.get_variable (es, "OPAPI_APIKEY");
            if (eapikey == null) {
                message ("NO OPAPI_APIKEY is defined so aborting");
                return;
            }

            try {
                var req = new Opapi.Request ();
                req.server = server;
                var projects = req.get_projects ();
                assert (projects.embedded != null);
                assert (projects.embedded.elements != null);
                assert (projects.embedded.elements.get_n_items () > 0);
                for (int i = 0; i < projects.embedded.elements.get_n_items (); i++) {
                    var o = projects.embedded.elements.get_item (i);
                    if (o is Opapi.Project) {
                        Opapi.Project p = (Opapi.Project) o;
                        if (p.name == "opapitest") {
                            message ("Project: %s", p.name);
                            Opapi.Form form = req.project_form (p);
                            assert (form.embedded != null);
                            assert (form.embedded is Opapi.EmbeddedForm);
                            Opapi.EmbeddedForm e = (Opapi.EmbeddedForm) form.embedded;
                            assert (e.payload != null);
                            message ("******** Read Form Payload for project: %s",p.name);
                            message ("Output:\n%s", e.payload.print (true));
                            Opapi.ProjectForm fp = new Opapi.ProjectForm ();
                            fp.parse_variant (e.payload);
                            var dt = new GLib.DateTime.now ();
                            string str = "Updated at: %s".printf (dt.format_iso8601 ());
                            fp.description.raw = str;
                            var v1 = fp.to_variant ();
                            message ("******** Updated Project's data: %s",p.name);
                            message ("Output:\n%s", v1.print (true));
                            e.payload = v1;
                            var vf = form.to_variant ();
                            message ("Form Updated:\n%s", vf.print (true));
                            message ("******** Calling Validate Form for project: %s",p.name);
                            Opapi.Form nfp = req.project_validate_form (p, form);
                            var v2 = nfp.to_variant ();
                            message ("******** Read Response Form for updated project: %s",p.name);
                            message ("Form Response for Update:\n%s", v2.print (true));
                            // Add checks if there are errors in the form
                            message ("******** Calling Update Form for project: %s",p.name);
                            var vfp = fp.to_variant ();
                            message ("Updated Form:\n%s", vfp.print (true));
                            req.project_update (p, fp);
                            break;
                        }
                    }
                }
            } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
            }
        });

        return Test.run ();
    }
}

/*
 * Opapi Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2022 <esodan@gmail.com>
 *
 * libopapi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liboapi is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Tests.UnitTest {
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Test.init (ref args);
        Test.add_func ("/opapi/create/project/post",
        ()=>{
            string server = "https://community.openproject.com";
            string[] es = GLib.Environ.get ();
            string eserver = GLib.Environ.get_variable (es, "OPAPI_SERVER");
            if (eserver != null) {
                server = eserver;
            }
            string eapikey = GLib.Environ.get_variable (es, "OPAPI_APIKEY");
            if (eapikey == null) {
                message ("NO OPAPI_APIKEY is defined so aborting");
                return;
            }

            try {
                var req = new Opapi.Request ();
                req.server = server;
                var p = new Opapi.Project ();
                p.name = "opapitest";
                var np = req.create_project (p);
                var v = np.to_variant ();
                message ("Output:\n%s", v.print (true));
            } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
            }
        });

        return Test.run ();
    }
}

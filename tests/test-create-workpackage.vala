/*
 * Opapi Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2022 <esodan@gmail.com>
 *
 * libopapi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liboapi is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Tests.UnitTest {
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Test.init (ref args);
        Test.add_func ("/opapi/create/project/post",
        ()=>{
            string server = "https://community.openproject.com";
            string[] es = GLib.Environ.get ();
            string eserver = GLib.Environ.get_variable (es, "OPAPI_SERVER");
            if (eserver != null) {
                server = eserver;
            }

            string eapikey = GLib.Environ.get_variable (es, "OPAPI_APIKEY");
            if (eapikey == null) {
                message ("No OPAPI_APIKEY is defined so aborting");
                return;
            }

            string pname = "opapitest";
            string eproject = GLib.Environ.get_variable (es, "OPAPI_PROYECT_NAME");
            if (eproject != null) {
                pname = eproject;
            }

            try {
                var req = new Opapi.Request ();
                req.server = server;
                var projects = req.get_projects ();

                Opapi.Project project = null;
                for (int i = 0; i < projects.embedded.elements.get_n_items (); i++) {
                    var o = projects.embedded.elements.get_item (i);
                    if (o is Opapi.Project) {
                        var p = (Opapi.Project) o;
                        if (p.name == pname) {
                            message ("Found Project: %s", pname);
                            project = p;
                            break;
                        }
                    }
                }

                var wp = new Opapi.WorkPackage ();
                wp.subject = "WorkPackage Test for opapitest project";
                var nwp = req.create_workpackage (project, wp);
                var v = nwp.to_variant ();
                message ("Output:\n%s", v.print (true));
            } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
            }
        });

        return Test.run ();
    }
}

/*
 * Opapi Unit Tests
 * Copyright (C) Daniel Espinosa Ortiz 2022 <esodan@gmail.com>
 *
 * libopapi is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * liboapi is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Tests.UnitTest {
    static int main (string[] args)
    {
        GLib.Intl.setlocale (GLib.LocaleCategory.ALL, "");
        Test.init (ref args);
        Test.add_func ("/opapi/request/project/workpackages",
        ()=>{
            string server = "https://community.openproject.com";
            string[] es = GLib.Environ.get ();
            string eserver = GLib.Environ.get_variable (es, "OPAPI_SERVER");
            if (eserver != null) {
                server = eserver;
            }

            try {
                var req = new Opapi.Request ();
                req.server = server;
                var projects = req.get_projects ();
                var v = projects.to_variant ();
                message ("Output:\n%s", v.print (true));
                assert (projects.embedded != null);
                assert (projects.embedded.elements != null);
                assert (projects.embedded.elements.get_n_items () > 0);
                bool workpgs = false;
                for (int i = 0; i < projects.embedded.elements.get_n_items (); i++) {
                    var o = projects.embedded.elements.get_item (i);
                    if (o is Opapi.Project) {
                        var p = (Opapi.Project) o;
                        assert (p.id != null);
                        assert (p.id.val != 0);
                        message ("Project: %s", p.name);
                        var wps = req.get_project_workpackages (p.id.val);
                        workpgs = wps.embedded.elements.get_n_items () > 0;
                        for (int j = 0; j < wps.embedded.elements.get_n_items (); j++) {
                            var wo = wps.embedded.elements.get_item (j);
                            if (wo is Opapi.WorkPackage) {
                                var wp = (Opapi.WorkPackage) wo;
                                message ("WorkPackage: %s", wp.subject);
                            }
                        }

                        if (workpgs) {
                            break;
                        }
                    }
                }
            } catch (GLib.Error e) {
                warning ("Error: %s", e.message);
            }
        });

        return Test.run ();
    }
}

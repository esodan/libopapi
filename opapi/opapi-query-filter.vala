/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A QueryFilter can be referenced by a filter instance
 * defined for a Query to denote the filtering applied
 * to the query’s work package results. This resource
 * is not an instance of an applicable filter but rather
 * the type an applicable filter can have.
 */
public class Opapi.QueryFilter : Opapi.Resource
{
    /**
     * Query Sort by's id
     */
    [Description (nick="id")]
    public string id { get ; set; }

    construct {
        __type = "QueryFilter";
    }
}

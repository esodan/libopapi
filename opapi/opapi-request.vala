/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Error domains for Requests
 */
public errordomain Opapi.RequestError {
    INVALID_RESPONSE_ERROR,
    THROWED_ERROR
}

/**
 * Authentication method
 */
public enum Opapi.AuthMethod {
    APIKEY,
    OAUTH,
    SESSION
}

/**
 * Request resources from an OpenProject API V3 server.
 *
 * Use OPAPI_SERVER environment variable to set the default
 * server to connect. This will be overrided by setting {@link server}
 * property.
 *
 * Use OPAPI_USER environment variable to set the user
 * for authentication to the server. Default is 'apikey'
 *
 * Use OPAPI_APIKEY environment variable to set the secret API key
 * for your use for authentication to the server.
 */
public class Opapi.Request : GLib.Object
{
    private string user = "apikey";
    private string apikey = "";
    private bool _is_authenticated = false;
    /**
     * OpenProject server's base URL
     *
     * Avoid to add api/v3 parts
     */
    public string server { get; set; }

    /**
     * Authenticated to the server
     */
    public bool is_authenticated { get { return _is_authenticated; } }

    /**
     * Authentication Method to use
     */
    public Opapi.AuthMethod auth_method { get; set; default = Opapi.AuthMethod.APIKEY; }

    construct {
        string[] es = GLib.Environ.get ();
        string eserver = GLib.Environ.get_variable (es, "OPAPI_SERVER");
        string euser = GLib.Environ.get_variable (es, "OPAPI_USER");
        string eapikey = GLib.Environ.get_variable (es, "OPAPI_APIKEY");
        if (eserver != null) {
            server = eserver;
        }
        if (euser != null) {
            user = euser;
        }
        if (eapikey != null) {
            apikey = eapikey;
        }
    }

    /**
     * User's name when autheticate. Dont's set if you ar using API key to access
     * the OpenProject server
     */
    public void set_user (string user) { this.user = user; }

    /**
     * User's API key to use to access
     * the OpenProject server.
     *
     * If you set it the user will be set to 'apikey' so
     * the server authenticates using the given key
     */
    public void set_api_key (string key) {
        apikey = key;
        user = "apikey";
    }

    private Soup.Session initialize_session (string url, Soup.Message msg)
    {
        Soup.Session session = new Soup.Session ();

        if (auth_method != Opapi.AuthMethod.APIKEY) {
            session.authenticate.connect ((msg, auth, retrying)=>{
                if (!retrying) {
                    debug ("Authenticating...");
                    auth.authenticate (this.user, this.apikey);
                    _is_authenticated = auth.is_authenticated;
                }
            });
        }

        return session;
    }

    /**
     * Get a resource using given URL. Resource will be converted to
     * {@link GLib.Variant} to be parsed.
     */
    public GLib.Variant call_get_sync (string url, GLib.Variant? options = null) throws GLib.Error
    {
        debug ("URL to use: '%s'", url);
        Soup.Message msg = new Soup.Message ("GET", url);
        Soup.Session session = initialize_session (url, msg);
#if DEBUG
        Soup.Logger logger = new Soup.Logger (Soup.LoggerLogLevel.HEADERS, -1);
	    session.add_feature (logger);
#endif
        if (auth_method == Opapi.AuthMethod.APIKEY && apikey != null && apikey != "") {
            string d = user + ":" + apikey;
            string encode = Base64.encode (d.data);
            msg.request_headers.append ("Authorization", "Basic " + encode);
        }

        if (options != null) {
            size_t l = 0;
            string d = Json.gvariant_serialize_data (options, out l);
            msg.request_body.append_take (d.data);
            msg.request_headers.set_content_type ("application/json", null);
        }

        session.send_message (msg);
        if (msg.response_body.data == null) {
            throw new Opapi.RequestError.INVALID_RESPONSE_ERROR (_("No data were receibed from the server"));
        }

        if (auth_method != Opapi.AuthMethod.APIKEY) {
            if (is_authenticated) {
                debug ("Authentication was valid!");
            } else {

                debug ("Authentication Fail!!");
            }
        }

        debug ("Data receibed:\n%s", (string) msg.response_body.data);
        GLib.Variant v = Json.gvariant_deserialize_data ((string) msg.response_body.data, -1, null);
        return v;
    }

    /**
     * Send request to create a resource using a POST method.
     * Resource should be provided as a {@link GLib.Variant}.
     */
    public GLib.Variant call_post_sync (string url, GLib.Variant? v, GLib.Variant? options = null) throws GLib.Error
    {
        debug ("URL to use: '%s'", url);
        Soup.Message msg = new Soup.Message ("POST", url);
        Soup.Session session = initialize_session (url, msg);
#if DEBUG
        Soup.Logger logger = new Soup.Logger (Soup.LoggerLogLevel.HEADERS, -1);
	    session.add_feature (logger);
#endif
        if (auth_method == Opapi.AuthMethod.APIKEY) {
            string d = user + ":" + apikey;
            string encode = Base64.encode (d.data);
            msg.request_headers.append ("Authorization", "Basic " + encode);
        }

        msg.request_headers.set_content_type ("application/json", null);

        if (v != null) {
            size_t l = 0;
            string d = Json.gvariant_serialize_data (v, out l);
            msg.request_body.append_take (d.data);
        }

        session.send_message (msg);

        if (auth_method != Opapi.AuthMethod.APIKEY) {
            if (is_authenticated) {
                debug ("Authentication was valid!");
            } else {

                debug ("Authentication Fail!!");
            }
        }

        if (msg.response_body.data == null) {
            throw new Opapi.RequestError.INVALID_RESPONSE_ERROR (_("No data were receibed from the server, after POST"));
        }

        debug ("Data receibed:\n%s", (string) msg.response_body.data);
        GLib.Variant va = Json.gvariant_deserialize_data ((string) msg.response_body.data, -1, null);
        return va;
    }

    /**
     * Send request to patch (update) a resource using a PATCH method.
     * Resource should be provided as a {@link GLib.Variant}.
     */
    public void call_patch_sync (string url, GLib.Variant v, GLib.Variant? options = null) throws GLib.Error
    {
        debug ("URL to use: '%s'", url);
        Soup.Message msg = new Soup.Message ("PATCH", url);
        Soup.Session session = initialize_session (url, msg);
#if DEBUG
        Soup.Logger logger = new Soup.Logger (Soup.LoggerLogLevel.HEADERS, -1);
	    session.add_feature (logger);
#endif
        if (auth_method == Opapi.AuthMethod.APIKEY) {
            string d = user + ":" + apikey;
            string encode = Base64.encode (d.data);
            msg.request_headers.append ("Authorization", "Basic " + encode);
        }

        size_t l = 0;
        string d = Json.gvariant_serialize_data (v, out l);
        debug ("Data sent:\n%s", d);
        msg.request_body.append_take (d.data);
        msg.request_headers.set_content_type ("application/json", null);

        session.send_message (msg);

        if (auth_method != Opapi.AuthMethod.APIKEY) {
            if (is_authenticated) {
                debug ("Authentication was valid!");
            } else {

                debug ("Authentication Fail!!");
            }
        }

        debug ("Data receibed:\n%s", (string) msg.response_body.data);
    }

    /**
     * Send request to create a resource using a POST method.
     * Resource should be provided as a {@link GLib.Variant}.
     */
    public void call_delete_sync (string url, GLib.Variant v, GLib.Variant? options = null) throws GLib.Error
    {
        debug ("URL to use: '%s'", url);
        Soup.Message msg = new Soup.Message ("DELETE", url);
        Soup.Session session = initialize_session (url, msg);
#if DEBUG
        Soup.Logger logger = new Soup.Logger (Soup.LoggerLogLevel.HEADERS, -1);
	    session.add_feature (logger);
#endif
        if (auth_method == Opapi.AuthMethod.APIKEY) {
            string d = user + ":" + apikey;
            string encode = Base64.encode (d.data);
            msg.request_headers.append ("Authorization", "Basic " + encode);
        }

        size_t l = 0;
        string d = Json.gvariant_serialize_data (v, out l);
        debug ("Data sent:\n%s", d);
        msg.request_body.append_take (d.data);
        msg.request_headers.set_content_type ("application/json", null);

        session.send_message (msg);

        if (auth_method != Opapi.AuthMethod.APIKEY) {
            if (is_authenticated) {
                debug ("Authentication was valid!");
            } else {

                debug ("Authentication Fail!!");
            }
        }

        debug ("Data receibed:\n%s", (string) msg.response_body.data);
    }

    /**
     * Retrieve the projects available to the current user's
     * API key
     */
    public Collection get_projects () throws GLib.Error
    {
        GLib.Variant v = call_get_sync (server + "/api/v3/projects");
        Collection col = new Collection ();
        col.parse_variant (v);
        return col;
    }

    /**
     * Retrieve the project's available workpackages
     */
    public Collection get_project_workpackages (int id) throws GLib.Error
    {
        GLib.Variant v = call_get_sync (server + "/api/v3/projects/%d/work_packages".printf (id));
        Collection col = null;
        if (GVo.Object.can_be_parsed_by (v, typeof (WorkPackageCollection))) {
            col = new WorkPackageCollection ();
        } else if (GVo.Object.can_be_parsed_by (v, typeof (Collection))) {
            col = new Collection ();
        }

        col.parse_variant (v);
        return col;
    }

    /**
     * Create a new project
     */
    public Project create_project (Project new_project) throws GLib.Error
    {
        GLib.Variant vp = new_project.to_variant ();
        var v = call_post_sync (server + "/api/v3/projects", vp);
        Project cp = new Project ();
        cp.parse_variant (v);
        return cp;
    }

    /**
     * Create a new work package for a given project
     */
    public WorkPackage create_workpackage (Project project, WorkPackage new_wp) throws GLib.Error
    {
        GLib.Variant vwp = new_wp.to_variant ();
        var v = call_post_sync (server + "/api/v3/projects/%d/work_packages".printf (project.id.val), vwp);
        WorkPackage cwp = new WorkPackage ();
        cwp.parse_variant (v);
        return cwp;
    }

    /**
     * Get a work package
     */
    public WorkPackage get_workpackage (int id) throws GLib.Error
    {
        var v = call_get_sync (server + "/api/v3/work_packages/%d".printf (id));
        Opapi.Error error = new Opapi.Error ();
        if (error.is_parseable (v)) {
            error.parse_variant (v);
            throw new RequestError.THROWED_ERROR (_("Can't locate WorkPackage: %d : %s"), id, error.message);
        }

        WorkPackage cwp = new WorkPackage ();
        cwp.parse_variant (v);
        return cwp;
    }

    /**
     * Delete a work package
     */
    public void delete_workpackage (WorkPackage wp) throws GLib.Error
    {
        GLib.Variant v = wp.to_variant ();
        call_delete_sync (server + "/api/v3/work_packages/%d".printf (wp.id.val), v);
    }

    /**
     * Request {@link Opapi.Form} for a given {@link Opapi.WorkPackage}
     */
    public Opapi.Form workpackage_form (WorkPackage wp) throws GLib.Error
    {
        var v = call_post_sync (server + "/api/v3/work_packages/%d/form".printf (wp.id.val), null);
        Opapi.Error error = new Opapi.Error ();
        if (error.is_parseable (v)) {
            error.parse_variant (v);
            throw new RequestError.THROWED_ERROR (_("Can't access to work package's form: %d : %s"), wp.id.val, error.message);
        }

        var f = new Opapi.Form ();
        f.parse_variant (v);
        return f;
    }

    /**
     * Request {@link Opapi.Form} for a given {@link Opapi.Project}
     */
    public Opapi.Form project_form (Project p) throws GLib.Error
    {
        var v = call_post_sync (server + "/api/v3/projects/%d/form".printf (p.id.val), null);
        Opapi.Error error = new Opapi.Error ();
        if (error.is_parseable (v)) {
            error.parse_variant (v);
            throw new RequestError.THROWED_ERROR (_("Can't access to project's form: %d : %s"), p.id.val, error.message);
        }

        var f = new Opapi.Form ();
        f.parse_variant (v);
        return f;
    }

    /**
     * Request update a given {@link Opapi.WorkPackage} using
     * the date in {@link Opapi.Form}
     */
    public Opapi.Form workpackage_validate_form (Opapi.WorkPackage wp, Opapi.Form f) throws GLib.Error
    {
        var vd = f.to_variant ();
        var v = call_post_sync (server + "/api/v3/work_packages/%d/form".printf (wp.id.val), vd);
        Opapi.Error error = new Opapi.Error ();
        if (error.is_parseable (v)) {
            error.parse_variant (v);
            throw new RequestError.THROWED_ERROR (_("Form validation error for work package values: %d : %s"), wp.id.val, error.message);
        }

        var wf = new Opapi.Form ();
        wf.parse_variant (v);
        return wf;
    }

    /**
     * Request validate a given {@link Opapi.Form} intented
     * to update a {@link Opapi.Project} using
     */
    public Opapi.Form project_validate_form (Opapi.Project p, Opapi.Form f) throws GLib.Error
    {
        var vd = f.to_variant ();
        var v = call_post_sync (server + "/api/v3/projects/%d/form".printf (p.id.val), vd);
        Opapi.Error error = new Opapi.Error ();
        if (error.is_parseable (v)) {
            error.parse_variant (v);
            throw new RequestError.THROWED_ERROR (_("Form validation error for project's values: %d : %s"), p.id.val, error.message);
        }

        var nf = new Opapi.Form ();
        nf.parse_variant (v);
        return nf;
    }

    /**
     * Request to update a given {@link Opapi.Form} intented
     * to update a {@link Opapi.Project} using
     */
    public void project_update (Opapi.Project p, Opapi.Project modified) throws GLib.Error
    {
        var vd = modified.to_variant ();
        call_patch_sync (server + "/api/v3/projects/%d".printf (p.id.val), vd);
    }

    /**
     * Request to update a given {@link Opapi.Form} intented
     * to update a {@link Opapi.Project} using
     */
    public void workpackage_update (Opapi.WorkPackage wp, Opapi.WorkPackage modified) throws GLib.Error
    {
        var vd = modified.to_variant ();
        call_patch_sync (server + "/api/v3/work_packages/%d".printf (wp.id.val), vd);
    }
}

/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Attachments are files that were uploaded to OpenProject.
 *
 * Each attachment belongs to a single container
 * (e.g. a work package or a board message).
 */
public class Opapi.Notification : Opapi.Resource
{
    /**
     * Notification's id
     */
    [Description (nick="id")]
    public int id { get ; set; }

    /**
     * The subject of the notification
     */
    [Description (nick="subject")]
    public string subject  { get ; set; }

    /**
     * The reason causing the notification
     */
    [Description (nick="reason")]
    public string reason { get ; set; }

    /**
     * Whether the notification is read
     */
    [Description (nick="readIAN")]
    public int64 read_ian { get ; set; }

    /**
     * Creation time
     */
    [Description (nick="createdAt")]
    public GLib.DateTime created_at { get ; set; }

    /**
     * Updated time
     */
    [Description (nick="updatedAt")]
    public GLib.DateTime updated_at { get ; set; }

    construct {
        __type = "Notification";
        links = new LinksNotification ();
    }
}


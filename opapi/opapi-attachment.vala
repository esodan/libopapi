/* llibopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Attachments are files that were uploaded to OpenProject.
 *
 * Each attachment belongs to a single container
 * (e.g. a work package or a board message).
 */
public class Opapi.Attachment : Opapi.Resource
{
    /**
     * Attachment's id
     */
    [Description (nick="id")]
    public int id { get ; set; }

    /**
     * The name of the file
     */
    [Description (nick="title")]
    public string title  { get ; set; }

    /**
     * The name of the uploaded file
     */
    [Description (nick="fileName")]
    public string file_name { get ; set; }

    /**
     * If status the default value
     */
    [Description (nick="fileSize")]
    public int64 file_size { get ; set; }

    /**
     * A user provided description of the file
     */
    [Description (nick="description")]
    public Formattable description { get ; set; }

    /**
     * The files MIME-Type as determined by the server
     */
    [Description (nick="contentType")]
    public string content_type { get ; set; }

    /**
     * A checksum for the files content
     */
    [Description (nick="digest")]
    public Digests digest { get ; set; }

    /**
     * A checksum for the files content
     */
    [Description (nick="createdAt")]
    public GLib.DateTime created_at { get ; set; }

    construct {
        __type = "Attachment";
        links = new LinksAttachment ();
    }
}


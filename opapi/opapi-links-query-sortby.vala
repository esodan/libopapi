/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A set of Links related to a query
 */
public class Opapi.LinksQuerySortBy : Opapi.Links
{
    /**
     * The QueryColumn to sort on.
     */
    [Description (nick="column")]
    public Link column { get ; set; }

    /**
     * The direction the QueryColumn is to be sorted in.
     * This property is identified by a
     * URI (urn:openproject-org:api:v3:queries:directions:asc or urn:openproject-org:api:v3:queries:directions:desc)
     * instead of by a URL.
     */
    [Description (nick="direction")]
    public Link direction { get ; set; }
}


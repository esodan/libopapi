/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Type of work package
 */
public class Opapi.Types : Opapi.Resource
{
    /**
     * Type's id
     */
    [Description (nick="id")]
    public int id { get ; set; }

    /**
     * Type name
     */
    [Description (nick="name")]
    public string name  { get ; set; }

    /**
     * The name of the uploaded file
     */
    [Description (nick="color")]
    public string color { get ; set; }

    /**
     * Sort index of the type
     */
    [Description (nick="position")]
    public int position { get ; set; }

    /**
     * If is default value
     */
    [Description (nick="isDefault")]
    public bool is_default { get ; set; }

    /**
     * Do tickets of this type represent a milestone?
     */
    [Description (nick="isMilestone")]
    public bool is_milestone { get ; set; }

    /**
     * Time of creation
     */
    [Description (nick="createdAt")]
    public GLib.DateTime created_at { get ; set; }

    /**
     * Time of the most recent change to the user
     */
    [Description (nick="updatedAt")]
    public GLib.DateTime updated_at { get ; set; }

    construct {
        __type = "Type";
    }
}


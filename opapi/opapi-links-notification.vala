/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A set of Links related
 */
public class Opapi.LinksNotification : Opapi.Links
{
    /**
     * The project containng the resource
     */
    [Description (nick="project")]
    public Link project { get ; set; }

    /**
     * The user that caused the notification
     */
    [Description (nick="actor")]
    public Link actor { get ; set; }

    /**
     * The resource the notification belongs to
     */
    [Description (nick="resource")]
    public Link resource { get ; set; }

    /**
     * The journal the notification belongs to
     */
    [Description (nick="activity")]
    public Link activity { get ; set; }
}

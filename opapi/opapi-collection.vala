/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Opapi.Collection : Opapi.Resource
{
    /**
     * Collection property. The total amount of elements available in the collection
     */
    [Description (nick="total")]
    public int total { get ; set; }

    /**
     * Collection property. Amount of elements that a response will hold
     */
    [Description (nick="pageSize")]
    public int page_size { get ; set; }

    /**
     * Collection property. Actual amount of elements in this response
     */
    [Description (nick="count")]
    public int count { get ; set; }

    /**
     * Collection property. The page number that is requested from paginated collection
     */
    [Description (nick="offset")]
    public int offset { get ; set; }

    /**
     * Collection property. Summarized information about aggregation groups
     */
    [Description (nick="groups")]
    public GVo.Object groups { get ; set; }

    /**
     * Aggregations of supported values for elements of the collection
     */
    [Description (nick="totalSums")]
    public GVo.Object total_sums { get ; set; }

    construct {
        __type = "Collection";
        links = new Opapi.LinksCollection ();
    }
}

/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Time entries are either linked to a work package or to a project.
 *
 * If they are linked to a project, the work package reference is empty.
 * If they are linked to a work package, the project reference is filled
 * up automatically to point to the work package’s project.
 */
public class Opapi.TimeEntry : Opapi.Resource
{
    /**
     * Time Entry's id
     */
    [Description (nick="id")]
    public int id { get ; set; }

    /**
     * A text provided by the user detailing the time entry
     */
    [Description (nick="comment")]
    public string comment  { get ; set; }

    /**
     * The date the expenditure is booked for
     */
    [Description (nick="spentOn")]
    public GLib.Date spent_on { get ; set; }

    /**
     * The time quantifying the expenditure
     */
    [Description (nick="hours")]
    public GVo.Duration hours { get ; set; }

    /**
     * The time the time entry was created
     */
    [Description (nick="createdAt")]
    public GLib.DateTime created_at { get ; set; }

    /**
     * The time the time entry was last updated
     */
    [Description (nick="updatedAt")]
    public GLib.DateTime updated_at { get ; set; }

    construct {
        __type = "TimeEntry";
        links = new LinksTimeEntry ();
    }
}


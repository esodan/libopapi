/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Represent a work package
 */
public class Opapi.WorkPackage : Opapi.Resource
{
    /**
     * Work package id
     */
    [Description (nick="id")]
    public GVo.Integer id { get ; set; }

    /**
     * The version of the item as used for optimistic locking
     */
    [Description (nick="lockVersion")]
    public int lock_version { get ; set; }

    /**
     * Work package subject
     */
    [Description (nick="subject")]
    public string subject { get ; set; }

    /**
     * Name of the work package’s type
     */
    [Description (nick="type")]
    public string type_name { get ; set; }

    /**
     * The work package description
     */
    [Description (nick="description")]
    public Formattable description { get ; set; }

    /**
     * If false (default) schedule automatically.
     */
    [Description (nick="scheduleManually")]
    public bool schedule_manually { get ; set; }

    /**
     * Scheduled beginning of a work package
     */
    [Description (nick="startDate")]
    public GVo.Date start_date { get ; set; }

    /**
     * Scheduled end of a work package
     */
    [Description (nick="dueDate")]
    public GVo.Date due_date { get ; set; }

    /**
     * Date on which a milestone is achieved
     */
    [Description (nick="date")]
    public GVo.Date date { get ; set; }

    /**
     * Similar to start date but is not set by a
     * client but rather deduced by the work packages’s
     * descendants. If manual scheduleManually is active,
     * the two dates can deviate.
     */
    [Description (nick="derivedStartDate")]
    public GVo.Date derived_start_date { get ; set; }

    /**
     * Similar to due date but is not set by
     * a client but rather deduced by the
     * work packages’s descendants.
     * If manual scheduleManually is active,
     * the two dates can deviate.
     */
    [Description (nick="derivedDueDate")]
    public GVo.Date derived_due_date { get ; set; }

    /**
     * Time a work package likely needs to be
     * completed excluding its descendants
     */
    [Description (nick="estimatedTime")]
    public GVo.Duration estimated_time { get ; set; }

    /**
     * Time a work package likely needs to be
     * completed including its descendants
     */
    [Description (nick="derivedEstimatedTime")]
    public GVo.Duration derived_estimated_time { get ; set; }

    /**
     * The time booked for this work package
     * by users working on it
     */
    [Description (nick="spentTime")]
    public GVo.Duration spent_time { get ; set; }

    /**
     * Amount of total completion for a work package.
     *
     * 0 <= x <= 100; Cannot be set for parent work packages
     */
    [Description (nick="percentageDone")]
    public uint percentage_done { get ; set; }

    /**
     * Time of creation
     */
    [Description (nick="createdAt")]
    public GVo.DateTime created_at { get ; set; }

    /**
     * Time of the most recent change to the work package
     */
    [Description (nick="updatedAt")]
    public GVo.DateTime updated_at { get ; set; }

    construct {
        __type = "WorkPackage";
        links = new Opapi.LinksWorkPackage ();
    }
}

public class Opapi.WorkPackageCollection : Opapi.Collection
{
    construct {
        __type = "WorkPackageCollection";
        links = new Opapi.LinksCollection ();
    }
}



/**
 * Use this object to parse objects provided by a {@link Opapi.Form}
 */
public class Opapi.WorkPackageForm : Opapi.WorkPackage
{
    construct {
        links = null;
    }
}

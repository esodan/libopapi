/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Object to hold a list of elements in a resource
 */
public class Opapi.Embedded : GLib.Object, GVo.Object
{
    /**
     * The document type
     */
    [Description (nick="elements")]
    public GVo.ContainerList elements { get; set; }


    construct {
        elements = new GVo.ContainerList ();
        // Make sure to add all objects types that will be holded by this container
        elements.add_parseable_type (typeof (Opapi.WorkPackage));
        elements.add_parseable_type (typeof (Opapi.Project));
    }



    internal virtual GVo.Object? generics_create_from_variant (GLib.Variant v)
    {
        return GVo.Object.create_from_variant (v);
    }
}

/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Object to hold a list of elements in a resource
 */
public class Opapi.EmbeddedForm : Opapi.Embedded
{
    /**
     * The payload contains an edited version of the resource
     * that will be modified when committing the form. This
     * representation contains all writable properties of the
     * resource and reflects all changes that the latest call
     * to validate included, thereby acting as a preview for
     * the changes.
     *
     * In case the client tries to set the value to something
     * invalid, the invalid change is also reflected here. However
     * a validation error (see below) indicates that a commit of
     * this payload would fail.
     *
     * It might happen that setting one property affects the
     * allowed values for another property. Thus by changing a
     * property A the current value of another property B might
     * become invalid. If the client did not yet touch the value
     * of B, the payload will contain a default value for that
     * property. Nevertheless the client will also receive an
     * appropriate validation error for value B.
     *
     * The content of this element can be used as a template
     * for the request body of a call to validate or commit.
     *
     * A call to validate and commit does not need to include
     * all properties that were defined in the payload section.
     * It is only necessary to include the properties that you
     * want to change, as well as the lockVersion if one is
     * present. However you may include all the properties
     * sent in the payload section.
     */
    [Description (nick="payload")]
    public GLib.Variant payload { get; set; }

    /**
     * The schema embedded in a form is a normal schema
     * describing the underlying resource. However, the
     * embedded schema can change with each revalidation
     * of the form. For example it might be possible, that
     * changing the type of a work package affects its
     * available properties, as well as possible values for
     * certain properties. As this makes the embedded schema
     * very dynamic, it is not included as a static link.
     */
    [Description (nick="schema")]
    public GVo.MapString schema { get; set; }

    /**
     * Like a schema the validation errors build a dictionary
     * where the key is a property name. Each value is an error
     * object that indicates the error that occurred validating
     * the corresponding property. There are only key value pairs
     * for properties that failed validation, the element is empty
     * if all validations succeeded.
     *
     * However note that even in the case of validation errors,
     * the response you receive from the form endpoint will be an
     * HTTP 200. That is because the main purpose of a form is
     * helping the client to sort out validation errors.
     */
    [Description (nick="validationErrors")]
    public GVo.MapString validation_errors { get; set; }

    /**
     * Form resources may have an additional _meta object
     * that contains parameters to be sent together with the
     * resource, but that do not belong to the resource itself.
     * For example, parameters on if and how to send notifications
     * for the action performed with the API request can be sent.
     *
     * Each individual endpoint will describe their meta properties,
     * if available
     */
    [Description (nick="_meta")]
    public GVo.Object meta { get; set; }

    construct {
        elements = null; // No elements property is present
    }
}


/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This API provides forms as a concept to aid in editing or
 * creating resources. The goal of forms is to:
 *
 * 1. make writable properties of a resource discoverable
 * 1. show to which values a property can be set
 * 1. validate changes to a resource and indicate validation errors
 *
 * These benefits aside, a client can freely choose to immediately
 * edit a resource without prior validation by a form. In the case
 * of an invalid request the edit will fail and return appropriate
 * errors nevertheless.
 *
 * A form is associated to a single resource and aids in performing
 * changes on that resource. When posting to a form endpoint with an
 * empty request body or an empty JSON object, you will receive an
 * initial form for the associated resource. Subsequent calls to the
 * form should contain a single JSON object as described by the form.
 *
 * {@link Opapi.Resource.embedded} is a {@link Opapi.EmbeddedForm} object with a
 * {@link Opapi.EmbeddedForm.payload} with a {@link GLib.Variant} object
 * suitable to be parsed to required object. For example, calling
 * {@link Opapi.Request.project_form} will return a {@link Opapi.Form} that
 * its {@link EmbeddedForm.payload} should be parsed to a {@link Opapi.Project}
 */
public class Opapi.Form : Opapi.Resource
{
    /**
     * Validate changes, show errors and allowed values
     * for changed resource
     */
    [Description (nick="validate")]
    public Link validate { get ; set; }

    /**
     * Actually perform changes to the resource
     */
    [Description (nick="commit")]
    public Link commit  { get ; set; }

    /**
     * Sort index of the status
     */
    [Description (nick="previewMarkup")]
    public Link preview_markup { get ; set; }

    /**
     * If status the default value
     */
    [Description (nick="createdAt")]
    public GLib.DateTime create_at { get ; set; }

    construct {
        __type = "Form";
        links = new LinksForm ();
        embedded = new EmbeddedForm ();
    }
}


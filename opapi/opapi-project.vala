/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Opapi.Project : Opapi.Resource
{
    /**
     * Projects’ id
     */
    [Description (nick="id")]
    public GVo.Integer id { get ; set; }

    /**
     * Project's identifier
     */
    [Description (nick="identifier")]
    public string identifier { get ; set; }

    /**
     * Project's name
     */
    [Description (nick="name")]
    public string name { get ; set; }

    /**
     * Indicates whether the project is currently active or already archived
     */
    [Description (nick="active")]
    public bool active { get ; set; default = true; }

    /**
     * A text detailing and explaining why the project has the reported status
     */
    [Description (nick="statusExplanation")]
    public Formattable status_explanation { get ; set; }

    /**
     * Indicates whether the project is accessible for everybody
     */
    [Description (nick="public")]
    public bool is_public { get ; set; }

    /**
     * Projec's description
     */
    [Description (nick="description")]
    public Opapi.Formattable description { get ; set; }

    /**
     * Time of creation. Read only
     */
    [Description (nick="createdAt")]
    public GVo.DateTime created_at { get ; set; }

    /**
     * Time of the most recent change to the project. Read only
     */
    [Description (nick="updatedAt")]
    public GVo.DateTime updated_at { get ; set; }

    construct {
        __type = "Project";
        links = new Opapi.LinksProject ();
    }
}

/**
 * Use this object to parse objects provided by a {@link Opapi.Form}
 */
public class Opapi.ProjectForm : Opapi.Project
{
    construct {
        links = null;
    }
}

/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * This object set {@link Opapi.Resource.object_type} property to 'Error'
 */
public class Opapi.Error : Opapi.Resource
{
    /**
     * The errorIdentifier serves as a unique (and machine readable) identifier for a specific error cause
     * 
     * 1. There may be multiple possible error identifiers per HTTP status code
     * 1. There may be multiple possible HTTP status codes per error identifier
     * 1. The “List of Error Identifiers” defines the possible mappings between HTTP status and error identifier
     */
    [Description (nick="errorIdentifier")]
    public string error_identifier { get ; set; }
    /**
     * Contains embedded resources
     */
    [Description (nick="message")]
    public string message { get ; set; }

    construct {
        __type = "Error";
    }
}

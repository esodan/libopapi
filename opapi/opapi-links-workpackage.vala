/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A set of Links related to work packages
 */
public class Opapi.LinksWorkPackage : Opapi.Links
{
    /**
     * The schema of this work package
     */
    [Description (nick="schema")]
    public Link schema { get ; set; }

    /**
     * Array of all visible ancestors of the work package,
     * with the root node being the first element
     */
    [Description (nick="ancestors")]
    public GVo.Container ancestors { get ; set; }

    /**
     * The files attached to this work package
     */
    [Description (nick="attachments")]
    public Link attachments { get ; set; }

    /**
     * The person that created the work package
     */
    [Description (nick="author")]
    public Link author { get ; set; }

    /**
     * The person that is intended to work on the work package
     */
    [Description (nick="assignee")]
    public Link assignee { get ; set; }

    /**
     * All users that can be added to the work package as watchers
     */
    [Description (nick="availableWatchers")]
    public Link available_watchers { get ; set; }

    /**
     * The budget this work package is associated to
     */
    [Description (nick="budget")]
    public Link budget { get ; set; }

    /**
     * The category of the work package
     */
    [Description (nick="category")]
    public Link category { get ; set; }

    /**
     * Array of all visible children of the work package
     */
    [Description (nick="children")]
    public GVo.Container children { get ; set; }

    /**
     * Parent work package
     */
    [Description (nick="parent")]
    public Link parent { get ; set; }

    /**
     * The priority of the work package
     */
    [Description (nick="priority")]
    public Link priority { get ; set; }

    /**
     * The project to which the work package belongs
     */
    [Description (nick="project")]
    public Link project { get ; set; }

    /**
     * The person that is responsible for the overall outcome
     */
    [Description (nick="responsible")]
    public Link responsible { get ; set; }

    /**
     * Relations this work package is involved in
     */
    [Description (nick="relations")]
    public Link relations { get ; set; }

    /**
     * Revisions that are referencing the work package
     */
    [Description (nick="revisions")]
    public Link revisions { get ; set; }

    /**
     * The current status of the work package
     */
    [Description (nick="status")]
    public Link status { get ; set; }

    /**
     * All time entries logged on the work
     * package.
     *
     * Please note that this is a link
     * to an HTML resource for now and as such,
     * the link is subject to change.
     */
    [Description (nick="timeEntries")]
    public Link time_entries { get ; set; }

    /**
     * The type of the work package
     */
    [Description (nick="type")]
    public Link workpackage_type { get ; set; }

    /**
     * The version associated to the work package
     */
    [Description (nick="version")]
    public Link version { get ; set; }

    /**
     * All users that are currently watching
     * this work package
     */
    [Description (nick="watchers")]
    public Link watchers { get ; set; }

    /**
     * Attach a file to the WP
     */
    [Description (nick="addAttachment")]
    public Link add_attachment { get ; set; }

    /**
     * Post comment to WP
     */
    [Description (nick="addComment")]
    public Link add_comment { get ; set; }

    /**
     * Adds a relation to this work package.
     */
    [Description (nick="addRelation")]
    public Link add_relation { get ; set; }

    /**
     * Add any user to WP watchers
     */
    [Description (nick="addWatcher")]
    public Link add_watcher { get ; set; }

    /**
     * Collection of predefined changes that
     * can be applied to the work package
     */
    [Description (nick="customActions")]
    public Link custom_actions { get ; set; }

    /**
     * Post markup (in markdown) here to
     * receive an HTML-rendered response
     */
    [Description (nick="previewMarkup")]
    public Link preview_markup { get ; set; }

    /**
     * Remove any user from WP watchers
     */
    [Description (nick="removeWatcher")]
    public Link remove_watcher { get ; set; }

    /**
     * Remove current user from WP watchers
     */
    [Description (nick="unwatch")]
    public Link unwatch { get ; set; }

    /**
     * Form endpoint that aids in preparing
     * and performing edits on a WP
     */
    [Description (nick="update")]
    public Link update { get ; set; }

    /**
     * Directly perform edits on a work package
     */
    [Description (nick="updateImmediately")]
    public Link update_immediately { get ; set; }

    /**
     * Add current user to WP watchers
     */
    [Description (nick="watch")]
    public Link watch { get ; set; }

    construct {
        ancestors = new GVo.ContainerHashList.for_type (typeof (Link));
        children = new GVo.ContainerHashList.for_type (typeof (Link));
    }
}

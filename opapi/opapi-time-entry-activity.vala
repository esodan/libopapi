/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Time entries are classified by an activity which is one item
 * of a set of user defined activities (e.g. Design, Specification,
 * Development).
 */
public class Opapi.TimeEntryActivity : Opapi.Resource
{
    /**
     * Time entries’ id
     */
    [Description (nick="id")]
    public int id { get ; set; }

    /**
     * The human readable name chosen for this activity.
     *
     * Maximum 30 characters
     */
    [Description (nick="name")]
    public string name  { get ; set; }

    /**
     * The rank the activity has in a list of activities
     */
    [Description (nick="position")]
    public int position { get ; set; }

    /**
     * Flag to signal whether this activity is the default activity
     */
    [Description (nick="default")]
    public bool @default { get ; set; }

    /**
     * A user provided description of the file
     */
    [Description (nick="description")]
    public Formattable description { get ; set; }

    construct {
        __type = "TimeEntriesActivity";
        links = new LinksTimeEntryActivity ();
    }
}


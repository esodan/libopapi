/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A query defines how work packages can be filtered and displayed.
 *
 * Clients can define a query once, store it, and use it later on
 * to load the same set of filters.
 *
 * A query that is not assigned to a project ("project": null) is
 * called a global query. Global queries filter work packages
 * regardless of the project they are assigned to. As such, a
 * different set of filters exists for those queries.
 */
public class Opapi.Query : Opapi.Resource
{
    /**
     * Query's id
     */
    [Description (nick="id")]
    public int id { get ; set; }

    /**
     * Query name
     */
    [Description (nick="name")]
    public string name  { get ; set; }

    /**
     * A set of QueryFilters which will be applied
     * to the work packages to determine the resulting work packages
     */
    [Description (nick="filters")]
    public GVo.Container filters { get ; set; }

    /**
     * Should sums (of supported properties) be shown?
     */
    [Description (nick="sums")]
    public bool sums { get ; set; }

    /**
     * Should the timeline mode be shown?
     */
    [Description (nick="timelineVisible")]
    public bool timeline_visible { get ; set; }

    /**
     * Which labels are shown in the timeline, empty when default
     */
    [Description (nick="timelineLabels")]
    public GVo.Object content_type { get ; set; } // FIXME: this object is not defined

    /**
     * Which zoom level should the timeline be rendered in?
     *public class Opapi.Query : Opapi.Resource
{
     * Options: days, weeks, months, quarters, years
     */
    [Description (nick="timelineZoomLevel")]
    public string timeline_zoom_level { get ; set; }

    /**
     * Which highlighting mode should the table have?
     *
     * Options: none, inline, status, priority, type
     */
    [Description (nick="highlightingMode")]
    public string highlighting_mode { get ; set; }

    /**
     * Should the hierarchy mode be enabled?
     */
    [Description (nick="showHierarchies")]
    public bool show_hierarchies { get ; set; }

    /**
     * Should the query be hidden from the query list?
     */
    [Description (nick="hidden")]
    public bool hidden { get ; set; }

    /**
     * Can users besides the owner see the query?
     */
    [Description (nick="public")]
    public bool @public { get ; set; }

    /**
     * Should the query be highlighted to the user?
     */
    [Description (nick="starred")]
    public bool starred { get ; set; }

    /**
     * Time of creation
     */
    [Description (nick="createdAt")]
    public GLib.DateTime created_at { get ; set; }

    /**
     * Time of the most recent change to the query
     */
    [Description (nick="updatedAt")]
    public GLib.DateTime updated_at { get ; set; }

    construct {
        __type = "Query";
        links = new LinksQuery ();
        filters = new GVo.ContainerHashList.for_type (typeof (QueryFilterInstance));
    }
}

/**
 * A QueryFilterInstance defines a filtering applied to the list of work packages
 */
public class Opapi.QueryFilterInstance : Opapi.Resource
{
    /**
     * QueryFilter name
     */
    [Description (nick="name")]
    public string name  { get ; set; }

    /**
     * List of used values
     */
    [Description (nick="values")]
    public GVo.ContainerList values  { get ; set; }

    construct {
        // FIXME: this is not correct, the type is defined by other place
        // may be is better to know first wich ones are present and create
        // derivative classes with the names of them
        __type = "QueryFilterIntance";
        links = new LinksQueryFilter ();
        values = new GVo.ContainerList ();
        values.add_parseable_type (typeof (GVo.String));
        values.add_parseable_type (typeof (GVo.Integer));
        values.add_parseable_type (typeof (Link));
    }
}


/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A set of Links related
 */
public class Opapi.LinksCollection : Opapi.Links
{
    /**
     * Templated link to change the page size, might change relative position
     */
    [Description (nick="changeSize")]
    public Link change_size { get ; set; }

    /**
     * Templated link to jump to a specified offset
     */
    [Description (nick="jumpTo")]
    public Link jump_to { get ; set; }

    /**
     * Link to retrieve the following page of elements (offset based)
     */
    [Description (nick="nextByOffset")]
    public Link next_by_offset { get ; set; }

    /**
     * Link to retrieve the preceding page of elements (offset based)
     */
    [Description (nick="previousByOffset")]
    public Link previous_by_offset { get ; set; }

    /**
     * Link to retrieve the elements following the current page (cursor based)
     */
    [Description (nick="nextByCursor")]
    public Link next_by_cursor { get ; set; }

    /**
     * Link to retrieve the elements preceding the current page (cursor based)
     */
    [Description (nick="previousByCursor")]
    public Link previous_by_crsor { get ; set; }
}

/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Attachments are files that were uploaded to OpenProject.
 *
 * Each attachment belongs to a single container
 * (e.g. a work package or a board message).
 */
public class Opapi.Priority : Opapi.Resource
{
    /**
     * Priority's id
     */
    [Description (nick="id")]
    public int id { get ; set; }

    /**
     * Priority name
     */
    [Description (nick="name")]
    public string name  { get ; set; }

    /**
     * Sort index of the priority
     */
    [Description (nick="position")]
    public int position { get ; set; }

    /**
     * Indicates whether this is the default value
     */
    [Description (nick="isDefault")]
    public bool is_default { get ; set; }

    /**
     * Indicates whether the priority is available
     */
    [Description (nick="isActive")]
    public bool is_active { get ; set; }

    construct {
        __type = "Priority";
    }
}


/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A set of Links related to a query
 */
public class Opapi.LinksQuery : Opapi.Links
{
    /**
     * The user that owns this query
     */
    [Description (nick="user")]
    public Link user { get ; set; }

    /**
     * The project on which this query operates
     */
    [Description (nick="project")]
    public Link project { get ; set; }

    /**
     * Ordered list of QueryColumns. The columns,
     * when mapped to WorkPackage properties determine
     * which WorkPackage properties to display
     */
    [Description (nick="columns")]
    public GVo.Container columns { get ; set; }

    /**
     * List of QueryColumns that should get highlighted
     * when highlightingMode is set to inline.
     *
     * Only with valid Enterprise Token available:
     */
    [Description (nick="highlightedAttributes")]
    public GVo.Container highlighted_attributes { get ; set; }

    /**
     * Ordered list of QuerySortBys. Indicates the WorkPackage
     * property the results will be ordered by as well as the direction
     */
    [Description (nick="sortBy")]
    public GVo.Container sort_by { get ; set; }

    /**
     * The WorkPackage property results of this query are grouped by
     */
    [Description (nick="groupBy")]
    public Link group_by { get ; set; }

    /**
     * The list of work packages returned by applying the
     * filters, sorting and grouping defined in the query
     */
    [Description (nick="results")]
    public Link results { get ; set; }

    /**
     * This query’s schema
     */
    [Description (nick="schema")]
    public Link schema { get ; set; }

    /**
     * Elevates the query to the status of ‘starred’
     */
    [Description (nick="star")]
    public Link star { get ; set; }

    /**
     * Removes the ‘starred’ status
     */
    [Description (nick="unstar")]
    public Link unstar { get ; set; }

    /**
     * Use the Form based process to verify
     * the query before persisting
     */
    [Description (nick="update")]
    public Link update { get ; set; }

    /**
     * Persist the query without using a Form based process
     * for guidance
     */
    [Description (nick="updateImmediately")]
    public Link update_immediately { get ; set; }

    construct {
        var c = new GVo.ContainerList ();
        c.add_parseable_type (typeof (QueryColumnProperty));
        c.add_parseable_type (typeof (QueryColumnRelation));
        c.add_parseable_type (typeof (QueryColumnRelationToType));
        c.add_parseable_type (typeof (QueryColumnRelationOfType));
        columns = c;
        var h = new GVo.ContainerList ();
        h.add_parseable_type (typeof (QueryColumnProperty));
        h.add_parseable_type (typeof (QueryColumnRelation));
        h.add_parseable_type (typeof (QueryColumnRelationToType));
        h.add_parseable_type (typeof (QueryColumnRelationOfType));
        highlighted_attributes = h;
        sort_by = new GVo.ContainerHashList.for_type (typeof (QuerySortBy));
    }
}

/**
 * A set of Links related to query filter
 */
public class Opapi.LinksQueryFilter : Opapi.Links
{
    /**
     * The filter type (QueryFilter) used
     */
    [Description (nick="filter")]
    public Link filter { get ; set; }

    /**
     * The operator (QueryOperator) used
     */
    [Description (nick="operator")]
    public Link operator { get ; set; }

    /**
     * The filter instance schema
     */
    [Description (nick="schema")]
    public Link schema { get ; set; }
}

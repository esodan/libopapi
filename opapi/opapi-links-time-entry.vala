/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A set of Links related
 */
public class Opapi.LinksTimeEntry : Opapi.Links
{
    /**
     * The project the time entry is bundled in.
     *
     * The project might be different from the
     * work package’s project once the workPackage
     * is moved.
     */
    [Description (nick="project")]
    public Link project { get ; set; }

    /**
     * The work package the time entry is created on
     */
    [Description (nick="workPackage")]
    public Link work_package { get ; set; }

    /**
     * The user the time entry tracks expenditures for
     */
    [Description (nick="user")]
    public Link user { get ; set; }

    /**
     * The time entry activity the time entry is categorized as
     */
    [Description (nick="activity")]
    public Link activity { get ; set; }

    /**
     * Directly perform edits on this time entry
     */
    [Description (nick="updateImmediately")]
    public Link update_immediately { get ; set; }

    /**
     * Form endpoint that aids in preparing and
     * performing edits on a TimeEntry
     */
    [Description (nick="update")]
    public Link update { get ; set; }

    /**
     * Delete this time entry
     */
    [Description (nick="delete")]
    public Link @delete { get ; set; }
}

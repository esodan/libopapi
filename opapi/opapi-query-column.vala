/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A QueryColumn can be referenced by a Query to denote the work package
 * properties the client should display for the work packages returned as
 * query results.
 *
 * The columns maps to the WorkPackage by the id property.
 * QueryColumns exist in three types: QueryColumn::Property,
 * QueryColumn::RelationToType and QueryColumn::RelationOfType.
 */
public class Opapi.QueryColumn : Opapi.Resource
{
    /**
     * Query column's id
     */
    [Description (nick="id")]
    public int id { get ; set; }

    /**
     * Query column's name
     */
    [Description (nick="name")]
    public string name  { get ; set; }

    construct {
        __type = "QueryColumn";
        links = new Links ();
    }
}

public class Opapi.QueryColumnProperty : Opapi.QueryColumn
{
    construct {
        __type = "QueryColumn::Property";
    }
}

public class Opapi.QueryColumnRelation : Opapi.QueryColumn
{
    construct {
        __type = "QueryColumn::Relation";
        links = new LinksQueryRelation ();
    }
}

public class Opapi.QueryColumnRelationToType : Opapi.QueryColumn
{
    construct {
        __type = "QueryColumn::RelationToType";
    }
}

public class Opapi.QueryColumnRelationOfType : Opapi.QueryColumn
{
    construct {
        __type = "QueryColumn::RelationOfType";
    }
}

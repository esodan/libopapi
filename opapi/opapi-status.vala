/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * OpenProject supports text formatting in Markdown
 */
public class Opapi.Status : Opapi.Resource
{
    /**
     * Status id
     */
    [Description (nick="id")]
    public int id { get ; set; }

    /**
     * Status name
     */
    [Description (nick="name")]
    public string name  { get ; set; }

    /**
     * Sort index of the status
     */
    [Description (nick="position")]
    public int position { get ; set; }

    /**
     * If status the default value
     */
    [Description (nick="isDefault")]
    public bool is_default { get ; set; }

    /**
     * are tickets of this status considered closed?
     */
    [Description (nick="isClosed")]
    public bool is_closed { get ; set; }

    /**
     * are tickets of this status read only?
     */
    [Description (nick="isReadonly")]
    public bool is_readonly { get ; set; }

    /**
     * The percentageDone being applied when changing to this status
     * as an integer where 0 <= x <= 100
     */
    [Description (nick="defaultDoneRatio")]
    public int default_done_ratio { get ; set; }

    construct {
        __type = "Status";
    }
}

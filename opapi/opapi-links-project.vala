/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A set of Links related to project objects
 */
public class Opapi.LinksProject : Opapi.Links
{
    /**
     * Categories available in this project
     */
    [Description (nick="categories")]
    public Link categories { get ; set; }

    /**
     * Types available in this project
     */
    [Description (nick="types")]
    public Link types { get ; set; }

    /**
     * Versions available in this project
     */
    [Description (nick="versions")]
    public Link versions { get ; set; }

    /**
     * Memberships in the project
     */
    [Description (nick="memberships")]
    public Link memberships { get ; set; }

    /**
     * Work Packages of this project
     */
    [Description (nick="workPackages")]
    public Link work_packages { get ; set; }

    /**
     * Parent project of the project
     */
    [Description (nick="parent")]
    public Link parent { get ; set; }

    /**
     * Denotes the status of the project, so whether
     * the project is on track, at risk or is having trouble.
     */
    [Description (nick="status")]
    public Link status { get ; set; }

    /**
     * Form endpoint that aids in updating this project
     */
    [Description (nick="update")]
    public Link update { get ; set; }

    /**
     * Directly update this project
     */
    [Description (nick="updateImmediately")]
    public Link update_immediately { get ; set; }

    /**
     * Delete this project
     */
    [Description (nick="delete")]
    public Link @delete { get ; set; }

    /**
     * Form endpoint that aids in preparing and
     * creating a work package
     */
    [Description (nick="createWorkPackage")]
    public Link create_work_package { get ; set; }

    /**
     * Directly creates a work package in the project
     */
    [Description (nick="createWorkPackageImmediately")]
    public Link create_work_package_immediately { get ; set; }
}

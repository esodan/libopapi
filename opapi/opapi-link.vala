/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Opapi.Link : GLib.Object, GVo.Object
{
    /**
     * URL to the referenced resource (might be relative)
     */
    [Description (nick="href")]
    public string href { get ; set; }

    /**
     * Representative label for the resource
     */
    [Description (nick="title")]
    public string title { get ; set; }

    /**
     * Contains embedded resources
     */
    [Description (nick="templated")]
    public bool templated { get ; set; }

    /**
     * The HTTP verb to use when requesting the resource
     */
    [Description (nick="method")]
    public string method { get ; set; }

    /**
     * The payload to send in the request to achieve the desired result
     */
    [Description (nick="payload")]
    public GVo.String payload { get ; set; }

    /**
     * An optional unique identifier to the link object
     */
    [Description (nick="identifier")]
    public GVo.String identifier { get ; set; }
}

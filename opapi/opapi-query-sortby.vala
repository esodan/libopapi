/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * A query defines how work packages can be filtered and displayed.
 *
 * Clients can define a query once, store it, and use it later on
 * to load the same set of filters.
 *
 * A query that is not assigned to a project ("project": null) is
 * called a global query. Global queries filter work packages
 * regardless of the project they are assigned to. As such, a
 * different set of filters exists for those queries.
 */
public class Opapi.QuerySortBy : Opapi.Resource
{
    /**
     * Query Sort by's id
     */
    [Description (nick="id")]
    public int id { get ; set; }

    /**
     * Query Sort by's name
     */
    [Description (nick="name")]
    public string name  { get ; set; }

    construct {
        __type = "QuerySortBy";
        links = new LinksQuerySortBy ();
    }
}

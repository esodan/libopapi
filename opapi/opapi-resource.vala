/* libopapi - OpenProject API implementation
 *
 * Copyright 2022 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Opapi.Resource : GLib.Object, GVo.Object
{
    protected string __type = "Resource";
    /**
     * The document type
     */
    [Description (nick="_type")]
    public string object_type {
        get {
            return __type;
        }
        set {}
    }

    /**
     * Contains links to resources
     */
    [Description (nick="_links")]
    public Links links { get ; set; }
    /**
     * Contains embedded resources
     */
    [Description (nick="_embedded")]
    public Embedded embedded { get ; set; }

    construct {
        embedded = new Embedded ();
        links = new Links ();
    }

    internal bool is_parseable (GLib.Variant v) {
        if (!v.is_container ()) {
            return false;
        }

        if (!v.check_format_string ("a{sv}", false)) {
            return false;
        }

        GLib.Variant vp = v.lookup_value ("_type", GLib.VariantType.STRING);
        if ( vp == null) {
            return false;
        }

        string n = vp.get_string ();
        if (n != object_type) {
            return false;
        }

        return true;
    }
}

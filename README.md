# Introduction

This library uses [GVo](https://gitlab.gnome.org/esodan/gvls/), the `GLib.Variant` to `GLib.Object` library,
to create an API to read/write JSON documents for [OpenProject API Specification](https://www.openproject.org/docs/api/).

# Documentation

Access Project's [Documentation](https://esodan.pages.gitlab.gnome.org/libopapi/)

# Dependencies

This library depends on [GVo](https://gitlab.gnome.org/esodan/libgvo), but also 
can handle it as a subproject.

Install directly `GVo` or enable subprojects option to use it as a development
environment.

# Build

Use following commands on a terminal in Linux to configure and build, using
GVo as a subproject:

```
meson -Dsubprojects=true build
cd build
ninja
```
 

